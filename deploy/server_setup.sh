#!/usr/bin/env bash

# TODO: Set to URL of git repo.
PROJECT_GIT_URL='https://gitlab.com/brevt/olimar.git'

PROJECT_BASE_PATH='/usr/local/apps'
VIRTUALENV_BASE_PATH='/usr/local/virtualenvs'

# Set Ubuntu Language
locale-gen en_GB.UTF-8

# Install Python, SQLite and pip
apt-get update
apt-get install -y python3-dev sqlite python-pip supervisor nginx git

# Upgrade pip to the latest version.
pip install --upgrade pip
pip install virtualenv

mkdir -p $PROJECT_BASE_PATH
git clone $PROJECT_GIT_URL $PROJECT_BASE_PATH/application_server_project

mkdir -p $VIRTUALENV_BASE_PATH
virtualenv --python=python3 $VIRTUALENV_BASE_PATH/content_api

source $VIRTUALENV_BASE_PATH/content_api/bin/activate
pip install -r $PROJECT_BASE_PATH/application_server_project/requirements.txt

# Run migrations
cd $PROJECT_BASE_PATH/application_server_project/src

# Setup Supervisor to run our uwsgi process.
cp $PROJECT_BASE_PATH/application_server_project/deploy/supervisor_content_api.conf /etc/supervisor/conf.d/content_api.conf
supervisorctl reread
supervisorctl update
supervisorctl restart content_api

# Setup nginx to make our application accessible.
cp $PROJECT_BASE_PATH/application_server_project/deploy/nginx_content_api.conf /etc/nginx/sites-available/content_api.conf
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/content_api.conf /etc/nginx/sites-enabled/content_api.conf
systemctl restart nginx.service

echo "DONE! :)"
