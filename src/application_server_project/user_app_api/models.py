from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

class Profile(models.Model):
    '''extends default user and adds new custom fields'''

    DEFAULT_POINTS = 50

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    points = models.IntegerField(default=DEFAULT_POINTS)

    def __str__(self):
        return "{}".format(self.user.username)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, **kwargs):
    instance.profile.save()