from django.shortcuts import render
from django.http import HttpRequest
from django.contrib.auth.models import User

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import AllowAny

from rest_framework_tracking.mixins import LoggingMixin

from . import serializers
from . import models
# from . import permissions
# Create your views here.

class ProfileViewSet(LoggingMixin, viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = models.Profile.objects.all()
    serializer_class = serializers.ProfileSerializer
    
    def create(self, request, *args, **kwargs):
        '''creates a profile for a given user, should only be done if request user is staff...'''
        if request.user.is_staff:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response({'status': 'permission denied, must be staff to create'})

    def retrieve(self, request, *args, **kwargs):
        '''Gets the profile data for a given user based on request user (../users/profile/:id)'''
        profile_object = self.get_object()
        serialized_profile_object = self.get_serializer(profile_object)

        request_user_id = request.user.id # the user ID of the user making the request
        requested_user = profile_object.user
        if request_user_id is requested_user.id:
        # the following data fields are from the user object from the Profile model 
            requested_user_id = requested_user.id
            requested_user_username = requested_user.username
            requested_user_active = requested_user.is_active
            requested_user_email = requested_user.email
            requested_user_staff = requested_user.is_staff

            user_data = {
                'id': requested_user_id,
                'req_id': request_user_id,
                'username': requested_user_username,
                'email': requested_user_email,
                'is_active': requested_user_active,
                'is_staff': requested_user_staff,
            }

            return Response({
            'status': 'ok', 
            'profile': serialized_profile_object.data, 
            'user_data': user_data
            })
        else:
            message = "Permission Denied: You can only acces your own data."
            return Response({'status': 'fail', 'message': message})

    def list(self, request, *args, **kwargs):
        '''returns the data of user by given query filter ../users/profile/?username='''
        requested_username = request.query_params.get('username', None)
        if requested_username:        
            requestor_userID = request.user.id
            found_user = User.objects.get(username=requested_username)

            if found_user.id is requestor_userID:
                profile_object = self.get_queryset().filter(user=found_user)
                serialized_profile_object = self.get_serializer(profile_object, many=True)
                
                user_data = {
                    'id': found_user.id,
                    'req_id': requestor_userID,
                    'username': found_user.username,
                    'email': found_user.email,
                    'is_active': found_user.is_active,
                    'is_staff': found_user.is_staff
                }
                return Response({'status': 'ok', 'profile': serialized_profile_object.data, 'user_data': user_data})
            else:
                message = "Permission Denied: You can only acces your own data."
                return Response({'status': 'fail', 'message': message})
        else:
            message = "No parameters given."
            return Response({'status': 'fail', 'message': message})

    def partial_update(self, request, *args, **kwargs):
        '''updates the points of user in given id'''
        profile_object = self.get_object()
        req_user = request.user.id
        user_object = User.objects.get(id=request.user.id)

        # checks to ensure the requestor is updating their own profile
        if user_object.id is profile_object.user.id:
            updated_points = request.data.get("new_points")
            previous_points = profile_object.points
            
            if updated_points:
                profile_object.points = updated_points
                profile_object.save()

            is_active_status = request.data.get("is_active")

            if is_active_status and is_active_status.lower() == 'false': 
                user_object.is_active = False
                user_object.save()                
            elif is_active_status and is_active_status.lower() == 'true':
                user_object.is_active = True
                user_object.save()                

            response_data = {
                'profile_id': profile_object.id,
                'request_user_id': request.user.id,
                'profile_user_id': profile_object.user.id,
                'previous_points': previous_points,
                'updated_points': profile_object.points,
                'is_active': user_object.is_active
            }
            return Response({'status': 'ok', 'updated_data': response_data})
        else:
            message = "Permission Denied: You can only partial-update your own points."
            return Response({'status': 'fail', 'message': message})
    
    def update(self, request, *args, **kwargs):
        if request.user.is_staff:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

            return Response(serializer.data)
        else:
            message = "Permission Denied: Only staff can only update entire profile. Try using PATCH instead of PUT."
            return Response({'status': 'fail', 'message': message})

    def destroy(self, request, *args, **kwargs):
        if request.user.is_staff:        
            instance = self.get_object()
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            message = "Permission Denied: Must have staff status to destory."
            return Response({'status': 'fail', 'message': message})