from rest_framework import authentication
from rest_framework import exceptions

from django.contrib.auth.models import User

import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth

from cacheops import cached_as

cred = credentials.Certificate('content_api/ServiceAcctCreds.json')
default_app = firebase_admin.initialize_app(cred)

class UserContentAuthentication(authentication.BaseAuthentication):
    '''Authenticates user through Firebase'''
    def authenticate(self, request):
        user_request_token = request.META.get('HTTP_AUTHORIZATION')

        if not user_request_token:
            raise exceptions.AuthenticationFailed("No Token Provided")

        # try:
        #     user_decoded_token = auth.verify_id_token(user_request_token)
        # except Exception as error:
        #     raise exceptions.AuthenticationFailed(error)
        
        # user_identifier = user_decoded_token['uid']
        firebase_user_profile = self.get_firebase_profile(user_request_token)
        user_identifier = firebase_user_profile['uid']
        user_email = firebase_user_profile['email']
        
        if not user_identifier:
            raise exceptions.AuthenticationFailed("That user does not exist")
        
        try:
            user = User.objects.get(username=user_identifier)
        except User.DoesNotExist:
            user = User.objects.create_user(username=user_identifier, email=user_email)

        # User.objects.get(username=user_identifier).cache()

        # User.objects.cache()

        return (user, None)
    
    @cached_as(User, timeout=120)
    def get_firebase_profile(self, request_token):
        '''Checks token with firebase and returns the Users uid'''
        try:
            user_decoded_token = auth.verify_id_token(request_token)
        except Exception as error:
            raise exceptions.AuthenticationFailed(error)
        
        return user_decoded_token