from django.conf.urls import url
from django.conf.urls import include

from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()

router.register('content', views.ContentItemViewSet)
router.register('feedback', views.ContentFeedbackViewSet)
router.register('ratings', views.RatingViewSet)

urlpatterns = [
    url(r'', include(router.urls))
]
