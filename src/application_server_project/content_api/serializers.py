from rest_framework import serializers
from . import models

class ContentItemSerializer(serializers.ModelSerializer):
    '''serializer for Content Items (articles)'''
    
    class Meta:
        model = models.ContentItem
        fields = ('id', 'source_url', 'image_url', 'title', 'description', 'rating_score', 'topic_tags', 'featured_tag', 'created_on', 'modified_on') 
        extra_kwargs = {'created_on': {'read_only': True}}

class ContentFeedbackSerializer(serializers.ModelSerializer):
    '''serializer for Feedback on content'''

    class Meta:
        model = models.ContentFeedback
        display_name = "test"
        fields = ('id', 'content_item', 'comment', 'quality_score', 'has_profanity', 'created_by', 'created_on', 'modified_on')
        # extra_kwargs = {'created_by': {'read_only': True}}

class RatingSerializer(serializers.ModelSerializer):
    '''Serializer for Rating content'''

    class Meta:
        model = models.Rating
        fields = ('id', 'content_item', 'user_id', 'rating')

    def create(self, validated_data):
        '''creates a new rating or updates existing if exists'''
        rating, created = models.Rating.objects.update_or_create(
            content_item=validated_data.get('content_item', None),
            user=validated_data.get('user', None),
            defaults={'rating': validated_data.get('rating', None)}
        )
        return rating