from django.shortcuts import render
from django.http import HttpRequest
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import DjangoModelPermissions

from rest_framework.permissions import IsAuthenticatedOrReadOnly

from rest_framework_tracking.mixins import LoggingMixin

from . import serializers
from . import models
from . import permissions

import firebase_admin
from firebase_admin import credentials
from firebase_admin import auth

from profanityfilter import ProfanityFilter

# import cacheops

class ContentItemViewSet(LoggingMixin, viewsets.ModelViewSet):
    '''Defines the create actions for the REST api'''
    permission_classes = (DjangoModelPermissions,)
    queryset = models.ContentItem.objects.all()
    serializer_class = serializers.ContentItemSerializer
    # models.ContentItem.objects.all().cache()

    def retrieve(self, request, *args, **kwargs):
        '''return single content object based on request PK (i.e. .../content/:id)'''

        content_object = self.get_object()
        serialized_content_object = self.get_serializer(content_object)

        return Response({'status': 'ok', 'articles': serialized_content_object.data})

    def list(self, request, *args, **kwargs):
        '''return all objects of given parameters, filtering by topic if provided in URL'''
        
        content_objects_topic = request.query_params.get('topic_tags', None)
        content_objects_featured = request.query_params.get('featured', None)

        if content_objects_featured == 'True':
            content_objects = self.get_queryset().filter(featured_tag='Yes')
        elif content_objects_topic is None:
            content_objects = self.get_queryset()
        else:
            content_objects = self.get_queryset().filter(topic_tags=content_objects_topic)
        
        serialized = self.get_serializer(content_objects, many=True)

        return Response({'topic': content_objects_topic, 'articles': serialized.data, 'total_results': len(serialized.data), 'status': 'ok'})

    def perform_create(self, serializer):
        '''Save the data for a new Content Item'''
        serializer.save()
class ContentFeedbackViewSet(LoggingMixin, viewsets.ModelViewSet):
    '''Defines actions for feedback in REST api'''
    # permission_classes = (DjangoModelPermissions,)  
    queryset = models.ContentFeedback.objects.all()
    serializer_class = serializers.ContentFeedbackSerializer
    # models.ContentFeedback.objects.all().cache()
    def retrieve(self, request, *args, **kwargs):
        '''return a single comment based on request PK (i.e. .../feedback/:id)'''
        feedback_object = self.get_object()
        serialized_feedback_object = self.get_serializer(feedback_object)

        return Response({'status': 'ok', 'feedback': serialized_feedback_object.data})

    def list(self, request, *args, **kwargs):
        '''return comments based on URL parameters (i.e. .../feedback/?content_item=:id or .../feedback/?created_by=:uid)'''

        feedback_objects_content = request.query_params.get('content_item', None)
        feedback_objects_user = request.query_params.get('created_by', None)

        '''
        the following logic block should probably be converted to a diff try/except block in the future to notify requestor
        if the object DoesNotExist, versus returning an empty object
        '''
        try:
            if feedback_objects_content and feedback_objects_user:
                feedback_objects = self.get_queryset().filter(
                    content_item=feedback_objects_content, 
                    created_by=feedback_objects_user,
                    has_profanity=False,
                    is_approved='Yes'
                )
            elif feedback_objects_user:
                feedback_objects = self.get_queryset().filter(
                    created_by=feedback_objects_user,
                    has_profanity=False,
                    is_approved='Yes'
                )
            elif feedback_objects_content:
                feedback_objects = self.get_queryset().filter(
                    content_item=feedback_objects_content,
                    has_profanity=False,
                    is_approved='Yes'
                )
            else:
                return Response({'status': 'fail', 'feedback': 'no parameters given - too many comments to return', 'total_results': 0})
        except ValueError:
            feedback_objects = None

        serialized_feedback = self.get_serializer(feedback_objects, many=True)
        error_message  = "None"
        for item in range(0, len(serialized_feedback.data)):
            feedback_item = serialized_feedback.data[item]
            try:
                user = auth.get_user(feedback_item["created_by"])
                username = user.display_name
            except auth.AuthError as error:
                error_message = "FIRAuth Error: {0}".format(error)
                username = "NONE"
            serialized_feedback.data[item]["display_name"] = username
        
        return Response({'status': 'ok', 'feedback': serialized_feedback.data, 'total_results': len(serialized_feedback.data), 'errors': error_message})

    def perform_create(self, serializer):
        '''save the data for new feedback'''
        submitted_comment = self.request.data['comment']
        pf = ProfanityFilter()

        serializer.save(created_by=self.request.user.username, has_profanity=pf.is_profane(submitted_comment))

class RatingViewSet(LoggingMixin, viewsets.ModelViewSet):
    '''Defines actions for rating content'''
    queryset = models.Rating.objects.all()
    serializer_class = serializers.RatingSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        '''save rating data and associated actions. This is used instead of create, which is just for adjusting data'''
        
        serializer.save(user=self.request.user)