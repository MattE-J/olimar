from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
# Create your models here.
class ContentItem(models.Model):
    '''A news article or piece of content'''
    TAG_CHOICES = (
        ('SCI', 'Science'),
        ('HTC', 'Healthcare'),
        ('POL', 'Politics'),
        ('GLB', 'Global'),
        ('ECN', 'Economics'),
        ('OTH', 'Other'),
    )

    FEATURED_TAG_CHOICES = (
        ('Yes', 'Yes'),
        ('No', 'No'),
    )

    RATING_DEFAULT = 0.75

    source_url = models.CharField(max_length=255, blank=False, unique=True)
    image_url = models.CharField(max_length=255, blank=False)
    title = models.CharField(max_length=255, blank=False)
    description = models.CharField(max_length=255, blank=False)
    topic_tags = models.CharField(choices=TAG_CHOICES, max_length=255)
    featured_tag = models.CharField(choices=FEATURED_TAG_CHOICES, default='No', max_length=255)
    rating_score = models.FloatField(default=RATING_DEFAULT)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        '''returns a human readable version of the instance'''
        return "{}".format(self.title)

class ContentFeedback(models.Model):
    '''Feedback given for a specific content item'''
    comment_max_length = 1000
    quality_score_default = 1.0

    APPROVED_TAG_CHOICES = (
        ('Yes', 'Yes'),
        ('No', 'No')
    )

    content_item = models.ForeignKey(ContentItem, on_delete=models.CASCADE)
    comment = models.TextField(max_length=comment_max_length, blank=False)
    quality_score = models.FloatField(default=quality_score_default)
    has_profanity = models.BooleanField(default=False)
    is_approved = models.CharField(choices=APPROVED_TAG_CHOICES, default='No', max_length=255)
    approval_notes = models.CharField(max_length = comment_max_length, default="NONE")
    created_by = models.CharField(max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        '''returns a humand readable version of the feedback as the first characters of the comment'''
        comment_exerpt_length = 35

        comment_exerpt = self.comment[:comment_exerpt_length]

        return "{}".format(comment_exerpt)

class Rating(models.Model):
    '''A table that connects users with content they've rated'''
    SCORE_DEFAULT = 0.5

    content_item = models.ForeignKey(ContentItem, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.FloatField(default=SCORE_DEFAULT)

    def __str__(self):
        return "{}".format(self.content_item)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Rating, self).save()
        
        calculated_rating = Rating.objects.filter(content_item=self.content_item).aggregate(models.Count('user'), models.Avg('rating'))
        
        if calculated_rating['user__count'] >= 3:
            self.content_item.rating_score = calculated_rating['rating__avg']
            self.content_item.save()