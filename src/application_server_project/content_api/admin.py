from django.contrib import admin
from . import models
# Register your models here.

# admin.site.register(models.ContentItem)
# admin.site.register(models.ContentFeedback)
@admin.register(models.ContentFeedback)
class ContentFeedbackAdmin(admin.ModelAdmin):
    list_filter = ('is_approved', 'has_profanity')

@admin.register(models.ContentItem)
class ContentItemAdmin(admin.ModelAdmin):
    list_filter = ('topic_tags', 'featured_tag',)