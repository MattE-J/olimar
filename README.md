# BREVT Server Application
## Django Rest Framework

### Version: 0.0.1
---
### Structure & Settings
The official technical requirements and software versions shall be noted in the requirements.txt and vagrantfile files for the project.  
An overview of the technologies used for developing and deploying the application:
- VirtualBox (VB): Runs a virtual Operating System
- Vagrant: Runs a virual server with a VB OS
- virtualenv: Runs a virtual environment in the OS for standardized installs and versions
- Django: Version 2.0.2, pytz-2017.3
- Django Rest Framework: Version 3.7.7, package that includes many required features out-of-the-box

---

### API Calls
This is an overview of valid calls to the API

#### Authorization
All calls made to the API must have an authorization header, which is a token granted by the Firebase database. The token is time sensitive and will expire after a predetermined amount of time. The token should then be refreshed and the header of the request updated. The Django server authenticates this header every time an API call is made!


#### Content Items
Content can be called in 4 ways:
- All content - this returns all articles in the database
    - *server_address*/api/content/
- All content by Topic - this returns all articles, filtered by given Topic Tag
    - *server_address*/api/content/?topics_tags=*tag*
- Single article by ID
    - *server_address*/api/content/*id*/
- Featured articles
    - *server_address*/api/content/?featured=True

Adding new content:
- POSTing required fields:
    - image_url: a url for the image to be displayed for the article
    - topic_tags: a three character tag describing the content category
        - SCI - science
        - HTC - healthcare
        - ECN - economics
        - GLB - global
        - POL - politics
        - OTH - other (note- this doesn't have a specific category in the front-end app at this time)
    - source_url: the url for the article content
    - title: must be less than 255 characters, title of the content
    - description: will only store the first 255 characters, describes the content briefly
- Admin page can be used to add content

#### Feedback Items
Feedback is created with a link to a parent content ID, given during the POSTing of a new feedback item.    
Feedback must not include profane verbiage, and the API call shall only return feedback that has been flagged as reviewed via the admin panel, and FALSE for is_profane field.
Feedback can be called in 4 ways using GET method request:
- All feedback by User ID - this returns all comments filtered by the User ID (UID from Firebase)
    - *server_address*/api/feedback/?created_by=*userID*
    - Example Request: http://138.68.225.199/api/feedback/?created_by=ABC123
- All feedback by Content Item - this returns all comments filtered by the Content Item ID (which can be requested from the content API call)
    - *server_address*/api/feedback/?content_item=*contentID*
    - Example request: http://138.68.225.199/api/feedback/?content_item=1

- All feedback by User ID and Content Item - this returns all commetns filtered by User ID and Content Item ID
    - *server_address*/api/feedback/?content_item=*contentID*&created_by=*userID*
    - Example request: http://138.68.225.199/api/feedback/?content_item=1&created_by=ABC123

- Example response for both created_by/content_item filtering:
 
```javascript
{
    "feedback": [
        {
            "id": 1,
            "content_item": 1,
            "comment": "This is a test comment!!!!",
            "quality_score": 3,
            "created_by": "ABC123",
            "created_on": "2018-02-20T22:16:31.805467Z",
            "modified_on": "2018-02-20T22:16:31.805506Z"
        }
    ],
    "status": "ok",
    "total_results": 1
}
```
- Single feedback item by ID. This is will show an individual comment givin the feedback ID
    - *server_address*/api/feedback/*feedbackID*/
    - Example request: http://138.68.225.199/api/feedback/1/
    - Example Response:

```javascript
{
    "feedback": {
        "id": 1,
        "content_item": 1,
        "comment": "This is a test comment!!!!",
        "quality_score": 3,
        "created_by": "ABC123",
        "created_on": "2018-02-20T22:16:31.805467Z",
        "modified_on": "2018-02-20T22:16:31.805506Z"
    },
    "status": "ok"
}
```

Adding new feedback:
Creating a new comment programmatically is done by a POST request to the url: .../api/feedback/   
The request must contain a body that has the following fields
- Method: POST
- POSTing required fields:
    - comment: the comment text
    - created_by: any random character will do - the server auto assigned the username of the user in the request as the created_by field when saving
    - content_item: the ID of the content item that is receiving feedback. This must be a valid ID from django database!

Additionally, the Djagno Admin page can be used to add feedback

#### User Profile Items
Users are created when they send a request to the server, after they are authenticated by Firebase. At that point, a profile gets created for them in the user_app_api Django application that copies all the user data, plus sets a default points field. Here are the endpoints available:

- Get Profile data
    - Method: GET
    - *server_address*/users/profiles/?username=*firebase_uid*
    - Response Example:

```javascript
{
    "profile": [
        {
            "id": 3,
            "user": 1,
            "points": 50
        }
    ],
    "status": "ok",
    "user_data": {
        "is_active": true,
        "req_id": 1,
        "email": "test@test.com",
        "id": 1,
        "is_staff": false,
        "username": "vXg27HBQE0ZPbtxe1vawEjjXeM42"
    }
}
```
- Update the users' points
    - Method: PATCH
    - Body
        - new_points: integer of the value of the users' new points
    - *id* must be provided in the request, and is the **Profile ID**. This can be retrieved by GETing the users Profile data via the GET Profile API call
    - *server_address*/users/profiles/*id*/
- Update the users' 'active' status. This is an alternative to deleting the account and managing the cascade of associated deletions by instead marking the is no longer active
    - Method: PATCH
    - Body
        - is_active: "True" or "False"
    - *id* must be provided in the request, and is the **Profile ID**. This can be retrieved by GETing the users Profile data via the GET Profile API call
    - *server_address***/users/profiles/***id***/**

---